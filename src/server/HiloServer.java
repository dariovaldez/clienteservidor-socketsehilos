package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class HiloServer implements Runnable {

	private static EchoObject eo = new EchoObject();  
	   private static String myURL="localhost";
	   private static ServerSocket serverSocket = null;
	   private static Socket clientSocket = null;
	   private static BufferedReader is = null;
	   private static PrintWriter os = null;
	   private static String inputline = new String();
	   
	   private Thread hilo;
	   
	   public HiloServer (Socket socket) {
		   clientSocket = socket;
		   hilo = new Thread(this);
		   hilo.start();
	   }

	   @Override
	   public void run() {
		   System.out.println("Estableciendo comunicación con " + hilo.getName());
		   try {
			    is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	        	os = new PrintWriter(clientSocket.getOutputStream());
	        	inputline = is.readLine();
	        	while (!inputline.equals("fin")) {
	 			   String respuesta;
	 			   System.out.println("Mensaje del cliente: "+ inputline);	
	 		  	   
	 	           respuesta = eo.echo(inputline);
	 	           os.println(respuesta);
	 	           os.flush(); 
	 	           
	 	          os.close();
			      is.close();
				  serverSocket.close();
				  clientSocket.close();
	 	           }
	        	os.flush(); 
	        	//os.close();
		        //is.close();
			    //serverSocket.close();
			    //clientSocket.close(); 
	 		   		System.out.println("fin del programa :)");
	 			    
		} catch (Exception e) {
			// TODO: handle exception
		}
		   
	 }  	   
}