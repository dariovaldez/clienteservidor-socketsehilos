package server;

import java.net.InetAddress;
//import java.net.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.*;

class EchoServer {
   private static EchoObject eo = new EchoObject();  
   private static String myURL="localhost";
   //private static ServerSocket serverSocket = null;
   //private static Socket clientSocket = null;
   //private static BufferedReader is = null;
   //private static PrintWriter os = null;
   //private static String inputline = new String();
   
   

   public static void main(String[] args) {
	   
      //System.out.println("Mi primer mensaje = " + eo.myURL);
	   try {
           myURL=InetAddress.getLocalHost().getHostName();
           System.out.println(myURL);
      } catch (UnknownHostException e) {
           System.out.println("Unknown Host :" + e.toString());
           System.exit(1);
      }
	   System.out.println("------------ S E R V E R ---------------------");
	   
	   try {	   
		   int mPuerto = 4500;
		   ServerSocket servidor;
		   servidor = new ServerSocket(mPuerto);
		   while (true) {
				Socket clSocket = new Socket();
				clSocket = servidor.accept();
				new HiloServer(clSocket);
			}
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	   
      /*try {
           serverSocket = new ServerSocket(mPuerto);
      } catch (IOException e) {
           System.out.println(myURL + ": could not listen on port " + e.toString());
           System.exit(1);
      }
      System.out.println(myURL + ": EchoServer listening on port:" + mPuerto);

      try {
          boolean listening = true;
		  String respuesta;
		  long contador = 0;
          while(listening){
        	contador++;
        	System.out.println("------------ S E R V E R ---------------------");
        	clientSocket = serverSocket.accept();// 
            
            //IMPLEMENTAR: 
        	//1. Instanciar la variable "is" para lee del buffer el objeto socket
        	//2. Instanciar la variable "os" para escribir
        	//3. Obtener los datos de la lectura en la variable "inputline"
        	
        	is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        	os = new PrintWriter(clientSocket.getOutputStream());
        	inputline = is.readLine();
        	
         
            
            if(inputline != null){
            	System.out.println("Mensaje del cliente: "+ inputline );
            	//IMPLEMENTAR:
            	//1. Variable "respuesta" lee el eco
            	//2. Escribe respuesta "os"
            	//3. Enviar respuesta. metodo =>flush 
            	
            	respuesta = eo.echo(inputline);
            	os.println(respuesta);
            	os.flush();
            	
            }
            
            if(inputline.equals("fin")){
            	System.out.println("Saliendo del ciclo"); 
            	listening=false;
            }
            
          }

         
        } catch (IOException e) {
            System.err.println("Error sending/receiving" + e.getMessage());
            e.printStackTrace();
      }
      
      
    try {
    	os.close();
        is.close();
		clientSocket.close();
	    serverSocket.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     
      System.out.println("fin del programa");*/
    }
}