package server;

import java.net.*;
import java.text.*;
import java.util.*;
import rmi.EchoInt;


public class EchoObject implements EchoInt {
  String myURL="localhost";

  public EchoObject(){
     try {
          myURL=InetAddress.getLocalHost().getHostName();
     } catch (UnknownHostException e) {
          myURL="localhost";
     }
  }

  public String echo(String input){
     Date h = new Date();
     String fecha = DateFormat.getTimeInstance(3,Locale.getDefault()).format(h);
     String ret = myURL + ":" + fecha + "> " +  input;
     try {
           Thread.sleep(1500);  ret = ret + " (delay 1.5 segundo)";
     } catch (InterruptedException e) {}

     return ret;
  }
}
