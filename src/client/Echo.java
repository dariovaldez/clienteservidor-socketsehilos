package client;

import java.io.*;
/*import server.EchoObject; //para la primera forma*/


import rmi.EchoInt;

public class Echo {
  private static EchoInt eo;  //EchoInt es la interfaz
  
  public static void main(String[] args) {

	int mPuerto = 4500; //digite el puerto del servidor
	int PuertoPapu = 4200;
	//IMPLEMENTAR: 1.- Crear una instancia del stub "eo"
	//EchoObjectStub eo = new EchoObjectStub("localhost", mPuerto);
	EchoObjectStub eo = new EchoObjectStub("192.168.1.9", PuertoPapu);
    BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
    PrintWriter stdOut = new PrintWriter(System.out);    
    String input,output,fin;
    
    //Bucle que lee de teclado, invoca el eco y escribe respuesta en la pantalla:
	input="";
	fin="fin";
	output="";
	
	
	while(!input.equals(fin)) {
		try {
			System.out.println("----------- C L I E N T E ------------------");
			stdOut.println("Escriba cadena para invocar su eco... presione fin para salir");
			stdOut.flush(); //Enviar salida stdout.
	    	
	    	//IMPLEMENTAR: 
	    	//1.- Lee cadena introducida por teclado variable "input"
	    	//2.- Invoca la cadena leida metodo echo del stub 
	    	//3.- Escribe la respuesta print (salida del string) "stdOut"
	    	//4.- Enviar eco (salida del string.  metodo => flush) 
				input = stdIn.readLine();
				output = eo.echo(input);
				stdOut.println(output);
				stdOut.flush();	    	
		} catch (Exception e) {	
			
			}
	}
    System.out.println("Fin del programa :)");
  }
}