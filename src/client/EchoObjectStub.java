package client;

import java.net.*;
import java.io.*;

import rmi.EchoInt;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

public class EchoObjectStub implements EchoInt {

  private Socket echoSocket = null;
  private PrintWriter os = null;
  private BufferedReader is = null;
  private String host = "localhost";
  private int port = 7;
  private String output = "Error";
 
  Timeout tout=null;

  public EchoObjectStub() {
  }

 public EchoObjectStub(String host, int port) {
     this.host= host; 
     this.port =port;
     tout = new Timeout(5,this);
  }

  public String echo(String input) 
  {
	  
	  if (connect()==true)
		 { 
		  System.out.println("conexion exitosa");
		  
		  if (echoSocket != null && os != null && is != null) 
		    {
		    try 
		    	{
		         os.println(input);
		         os.flush();
		         output= is.readLine();
		    	} catch (IOException e) {System.err.println
		    		("I/O failed in reading/writing socket");}
		    }
		  }
	  else
			{ 
			 System.out.print("Conexion fallida");
    		 tout.cancel();
			}
	  
	  
	programDisconnection();
    return output;
  }

  private synchronized boolean connect() 
  {
	 //EJERCICIO: Implemente el metodo connect	  
	  try {
		  echoSocket = new Socket(host, port);
		  is = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
		  os = new PrintWriter(echoSocket.getOutputStream());
		  return true;
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println("ERROR 404");
	}
	  
	return false; 
		  	 
  }

  private synchronized void disconnect(){
	  
	  //EJERCICIO: Implemente el metodo disconnect
	
	  try {
		
		  is.close();
		  os.close();
		  echoSocket.close();
		  
	} catch (Exception e) {
		// TODO: handle exception
	}
	  
  }

  private synchronized void programDisconnection(){
    tout.start();
  }

  class Timeout {
     Timer timer;
     EchoObjectStub stub;
     int seconds;

     public Timeout (int seconds, EchoObjectStub stub) {
       this.seconds = seconds;
       this.stub = stub;
     }

     public void start() {
    	 timer=new Timer();
    	 timer.schedule(new TimeoutTask(), seconds*1000);
     }

     public void cancel() {
    	 timer.cancel();
     }

     class TimeoutTask extends TimerTask {
    	
    	 @Override
    	 public void run(){
    		 System.out.println("Desconexion:"+seconds);
 			 stub.disconnect();
 			 timer.cancel();
    		 //
    	 }
     }

   }
}
